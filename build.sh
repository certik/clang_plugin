CONDA_PREFIX=$CONDA_ENV_PATH

CLANG_LIBS="-lclangTooling -lclangToolingCore -lclangFrontendTool -lclangFrontend -lclangDriver -lclangSerialization -lclangCodeGen -lclangParse -lclangSema -lclangStaticAnalyzerFrontend -lclangStaticAnalyzerCheckers -lclangStaticAnalyzerCore -lclangAnalysis -lclangARCMigrate -lclangRewrite -lclangRewriteFrontend -lclangEdit -lclangAST -lclangLex -lclangBasic -lclangASTMatchers -lclang"

g++ -I$CONDA_PREFIX/include -L$CONDA_PREFIX/lib -Wl,-rpath=$CONDA_PREFIX/lib \
    `llvm-config --cxxflags --ldflags` \
    ClangCheck.cpp \
    $CLANG_LIBS `llvm-config --libs --system-libs` -o clang-check
