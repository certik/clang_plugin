CONDA_PREFIX=$CONDA_ENV_PATH

./clang-check ClangCheck.cpp -- clang++ -std=c++11 -I$CONDA_PREFIX/include \
    -I$CONDA_PREFIX/lib/clang/3.7.0/include/ \
    -D_GNU_SOURCE -D_DEBUG -D__STDC_CONSTANT_MACROS \
    -D__STDC_FORMAT_MACROS -D__STDC_LIMIT_MACROS -D_GNU_SOURCE
