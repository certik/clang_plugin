# Testing Clang Plugins

Install Clang 3.7 into a `clang` environment:

    conda create -n clang -c inducer akclangdev=3.7.0

Build and run:

    source activate clang
    ./build
    ./run

## Documentation

http://llvm.org/releases/3.7.0/tools/clang/docs/IntroductionToTheClangAST.html
